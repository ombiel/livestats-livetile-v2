var { LiveTile, registerTile, getLibraries, getCredentials } = require("@ombiel/exlib-livetile-tools");
var libraryCSS = require("../../css/library");
var screenLink = require("@ombiel/aek-lib/screen-link");
var _has = require("-aek/utils").has;
var _isEmpty = require("-aek/utils").isEmpty;
var cssClasses = require("../../css/animations");
var moment = require('moment');

// Declare variables for libraries scoped to this module
var $, _;

// Create a promise that resolves when these libraries are ready to use
var libsReady = getLibraries(["jquery", "lodash"]).then(function (libs) {
  [$, _] = libs; // Assign the returned libraries to the global scope for this module
});


class LibraryTile extends LiveTile {

  onReady() {
    this.showOriginalFace();
    this.setOptions();

    libsReady.then(() => {

      this.renderImmediately = this.render.bind(this);
      this.throttledRender = _.throttle(this.render.bind(this), 1000); // Debounce render so it doesn't get triggered more than once in a 1 sec period

      this.loanIndex = 0;
      // a fallback in case anything happens to getCredentials
      this.legacyCreds = { username:"", password:"" };

      getCredentials().then((creds) => {
        this.legacyCreds = creds;
        this.delegateRequests();
      }).catch(() => {
        // getCredentials can fail if a LOGIN web service endpoint isn't define in the client's app
        // but we still want to fire the services in case this a cmAuth implementation with any CL services
        this.delegateRequests();
      });

    });

  }

  delegateRequests() {
    let promiseList = [];
    promiseList.push(this.fetchData());
    promiseList.push(this.fetchLoans());
    promiseList.push(this.fetchRequests());

    Promise.all(promiseList).then(() => {
      this.getNextLoan();
    });
  }

  fetchData() {
    return new Promise((resolve) => {
      var url = screenLink("livestats-lt/data");
      // cmAuth
      // return this.ajax({ url: url })
      // non cmAuth
      this.ajax({ url: url, data:this.legacyCreds })
      .then((data) => {
        // console.warn("User data ::");
        // console.log(data);
        
        // Check to ensure data is being returned before rendering
        if (data) {
          this.response = data;
          // removed render() pending resolution of fetchLoan, which now always triggers throttledRender()
        }
      })
      .always(() => {
        // Refresh data based on set delay
        if (this.fetchDataTimer) {
          this.fetchDataTimer.stop();
        }

        this.fetchDataTimer = this.timer(this.refreshDelay, this.fetchData.bind(this));

        resolve(true);
      });
    })
  }

  fetchLoans() {
    return new Promise((resolve) => {
      var url = screenLink("livestats-lt/loans");
      // cmAuth
      // return this.ajax({ url: url })
      // non cmAuth
      this.ajax({ url: url, data:this.legacyCreds })
      .then((data) => {
        // console.warn("Loans ::");
        // console.log(data);

        // Check to ensure data is being returned before rendering
        if (data && data.item_loan) {
          this.loans = data.item_loan;
          this.dueSoon = this.loans.filter((loan) => {
            return moment(loan.due_date).isBefore(moment().add(this.dueSoonDays, 'd'));
          });
        } else {
          this.loans = [];
          this.dueSoon = [];
        }

      })
      .always(() => {
        // removed timer clause as we already have a loansData timer on getNextLoan
        if(!this.loans) {
          this.loans = [];
        }

        if(!this.dueSoon) {
          this.dueSoon = [];
        }

        resolve(true);
      });
    });
  }

  fetchRequests() {
    return new Promise((resolve) => {
      var url = screenLink("livestats-lt/requests");
      // cmAuth
      // return this.ajax({ url: url })
      // non cmAuth
      this.ajax({ url: url, data:this.legacyCreds })
      .then((data) => {
        // console.warn("Requests ::");
        // console.log(data);

        // Check to ensure data is being returned before rendering
        if (data && data.user_request) {
          this.requests = data.user_request;
        }
      })
      .always(() => {
        // Refresh data based on set delay
        if (this.fetchRequestsTimer) {
          this.fetchRequestsTimer.stop();
        }

        this.fetchRequestsTimer = this.timer(this.refreshDelay, this.fetchRequests.bind(this));

        resolve(true);
      });
    });
  }

  getNextLoan() {
    this.loanIndex++;
    this.loanIndex = this.loanIndex === this.dueSoon.length ? 0 : this.loanIndex;

    // cleaned up no data causing tile to re-render periodically
    if(this.dueSoon.length > 0) {
      this.loan = this.dueSoon[this.loanIndex];
      if (this.fetchOverdueTimer) {
        this.fetchOverdueTimer.stop();
      }

      this.fetchOverdueTimer = this.timer(this.nextLoanDelay, this.getNextLoan.bind(this));
      // setInterval(this.getNextLoan.bind(this), this.nextLoanDelay);
    } else {
      // attempt to check for loans at the next interval
      if (this.fetchLoansTimer) {
        this.fetchLoansTimer.stop();
      }

      this.fetchLoansTimer = this.timer(this.refreshDelay, this.fetchLoans.bind(this));
    }

    this.throttledRender();
  }

  render() {
    let response = this.response;
    let loansData = this.loan;
    
    let requests = this.requests;
    let total = requests ? requests.length : false;
    let requestsData;
    
    if(!_.isEmpty(this.requests)) {
      if(!total) {
        // Object
        requestsData = [];
        requestsData.push(requests);
      }
      else {
        // Array
        requestsData = requests;
      }
    }
    
    requestsData = requestsData ? requestsData.reduce(function(n, request) {
      return n + (request.request_status === "On Hold Shelf");
    }, 0) : 0;
    
    let loans = this.loans;
    let totalLoans = loans ? loans.length : false;
    let overdueData;
    
    if(!_.isEmpty(this.loans)) {
      if(!totalLoans) {
        // Object
        overdueData = [];
        overdueData.push(loans);
      }
      else {
        // Array
        overdueData = loans;
      }
    }
    
    overdueData = overdueData ? overdueData.reduce(function(n, loan) {
      return n + (moment(loan.due_date).isBefore(moment()));
    }, 0) : 0;
    
    // Tile Attributes
    let tileAttributes = this.getTileAttributes();
    
    // Tile background image set in runserver.yaml
    let backgroundImage = "";
    if (tileAttributes.image || tileAttributes.img) {
      let image = tileAttributes.image || tileAttributes.img;
      // updated so that we don't call a bad image / 404 on render
      backgroundImage = "background-image: url(" + image + ");";
    }
    
    let faceContainer = document.createElement('div');
    $(faceContainer).css({ height:"100%" });
    let content = document.createElement('div');
    content.setAttribute('style', `display:block !important; height:auto; background-repeat: no-repeat; background-size: auto; background-position: center center; width: 100%; height: 100%; ${backgroundImage} position: absolute !important; top:0; margin:0 !important;`);
    
    if (!response || _.isEmpty(response)) {
      // If there is no response the service returns an error desc
      
      // Do not show badges if there is no response data
      $(content).html(`
      <div class="${libraryCSS.libraryTile}">
        <div class="${libraryCSS.libraryTileInfo}" style="border: none">
        </div>
      </div>
      `);
      
    } else {
      let loans = "-";
      // let fees = "-";
      let requests = "-";
      let overdue = "-";

      if (_.has(response, "loans")) {
        loans = response.loans;
        if(typeof loans === "string") {
          loans = parseInt(loans);
        }
      } else {
        loans = 0;
      }

      // if (_has(response, "fees")) {
      //   fees = this.currencySymbol + parseFloat(response.fees).toFixed(2);
      // }

      if (overdueData >= 0) {
        overdue = overdueData;
      }

      if (requestsData >= 0) {
        requests = requestsData;
      }


      let loanClass = `${libraryCSS.infoBox}`;
      let requestClass = `${libraryCSS.infoBox}`;
      // let feeClass = `${libraryCSS.infoBox}`;
      let overdueClass = `${libraryCSS.infoBox}`;

      let loanCircleClass = `${libraryCSS.infoCircle}`;
      let requestCircleClass = `${libraryCSS.infoCircle}`;
      // let feeCircleClass = `${libraryCSS.infoCircle}`;
      let overdueCircleClass = `${libraryCSS.infoCircle}`;

      if (loans >= 0) {
        loanCircleClass += ` ${libraryCSS.gotStuff}`;
      }

      if (requests >= 0) {
        requestCircleClass += ` ${libraryCSS.gotStuff}`;
      }

      if (overdue >= 0) {
        overdueCircleClass += ` ${libraryCSS.gotStuff}`;
      }

      // if (parseInt(fees) >= 0) {
      //   feeCircleClass += ` ${libraryCSS.gotStuff}`;
      // }

      $(content).html(`
        <div class="${libraryCSS.libraryTile}" style="margin-top:40px;">
          <div class="${libraryCSS.libraryTileInfo}">
            <div>
              <div class="${loanClass}">
                <div class="${loanCircleClass}">${loans}</div>
                <h4 class="${libraryCSS.infoTitle}">${this.loansText}</h4>
              </div>
            </div>
            <div>
              <div class="${overdueClass}">
                <div class="${overdueCircleClass}">${overdue}</div>
                <h4 class="${libraryCSS.infoTitle}">${this.overdueText}</h4>
              </div>
            </div>
            <div>
              <div class="${requestClass}">
                <div class="${requestCircleClass}">${requests}</div>
                <h4 class="${libraryCSS.infoTitle}">${this.requestsText}</h4>
              </div>
            </div>
          </div>
        </div>
      `);

      $(content).appendTo(faceContainer);

      var data = document.createElement('div');
      $(data).html(loansData ? `<h7 style="top: 71%; left:8%; position:absolute;">${loansData.title}</br>
      Due ${moment(loansData.due_date).format('MMMM Do YYYY')}</h7>` : `<h7 style="bottom: 17%; left: 8%; position:absolute;">${this.noLoansText}</h7>`);
      data.setAttribute('style', "font-weight: bold; width: 95%; padding-left: 5%; color:#FFF; margin-top: 0 !important; text-align: left; font-size: small; height: 100%; position: absolute !important;");
      $(data).appendTo(faceContainer);

      this.setFace(faceContainer);
      
      $(faceContainer).addClass(cssClasses["slideIn"]);
      $(data).addClass(cssClasses.in).removeClass(cssClasses.out).css({ visibility: "visible" });
    }

  }

  setOptions() {
    var tileAttributes = this.getTileAttributes();
    var libraryOptions = tileAttributes.library || {};
    this.loansText = libraryOptions.loansText || "Loans";
    this.requestsText = libraryOptions.requestsText || "To pick up";
    this.feesText = libraryOptions.feesText || "Charges";
    this.overdueText = libraryOptions.overdueText || "Overdue";
    this.noLoansText = libraryOptions.noLoansText || "No loans requiring attention";
    this.error = libraryOptions.error || "Error";
    this.currencySymbol = libraryOptions.requestsText || "$";
    this.dueSoonDays = libraryOptions.dueSoonDays || 2;
    this.nextLoanDelay = libraryOptions.nextLoanDelay || 30000;
    this.refreshDelay = parseInt(tileAttributes.refreshDelay) || 600000;
  }

}

registerTile(LibraryTile, "libraryTile2");
